function PostHelperPrototype() {
	var self = this;
	
	self.uploadPhoto = function() 
	{
		// $.mobile.changePage("#popup_upload_photo", { role: "popup" });
		$("#popup_upload_photo").popup("open");
	};
	
	self.TYPE_USERPOST = 0, 
	self.TYPE_FACEBOOK = 1, 
	self.TYPE_TWITTER = 2, 
	self.TYPE_SCHEDULE = 3, 
	self.TYPE_LOCATION = 4, 
	self.TYPE_WEATHER = 5,
	self.TYPE_CONTACTS = 6;
    
	self.db = null;

	// Transaction error callback
	self.callbackError = function(tx, results) {
		console.log(results);
	};

	// Transaction success callback
	self.callbackSuccess = function (tx, results) {
		console.log(results);
	};
	
	self.init = function() {
	    self.db = window.openDatabase("MOMENT_DB", "1.0", "MOMENT_DB", 200000);
	    self.db.transaction(function (tx) {
			// tx.executeSql("DROP TABLE moment");
		    tx.executeSql("CREATE TABLE IF NOT EXISTS moment (" +
		        "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
		        "timestamp TIMESTAMP NOT NULL, " +
		        "type INTEGER(3) NOT NULL, " +
		        "locationString VARCHAR(255), " +
		        "longitude FLOAT, " +
		        "latitude FLOAT, " +
		        "weatherType INTEGER(6), " +
		        "weatherTemperature FLOAT, " +
		        "note VARCHAR(255))"
				, [], 
				function (tx, results) {
					self.list({
						callback: function(tx, results) {
							if (is_test && results.rows && results.rows.length == 0)
							{
								self.generateSample();
							}
							
							if (results.rows && results.rows.length > 0)
							{
								var html = '';
								for (var i = 0 ; i < results.rows.length ; i++)
								{
									html += self.rowToHtml(results.rows.item(i));
								}
								$("#page_main_content").prepend(html)
								$("#page_main_content > div").tsort({
									order: 'desc', 
									attr: 'id',
								});
							}
							
						},
					});
	    		}, self.callbackError);
		});
	};
	
	self.rowToHtml = function(row) {
		var buf = "";
		
		var id = "id_" + row.timestamp + "_" + row.id;
		
		switch (row.type)
		{
		case self.TYPE_USERPOST:
			buf += '<div class="log ui-shadow" id="' + id + '">';
			buf += '	<div class="log_date">' + row.timestamp + '</div>';
			buf += '	<div class="log_weather">' + row.weatherType + ' ' + row.weatherTemperature + '</div>';
			buf += '		<div class="log_content">';
			buf += row.note;
			buf += '		</div>';
			buf += '		<div class="log_picture">';
			buf += '			<img src="img/flowers/1.png" />';
			buf += '			<img src="img/flowers/2.png" />';
			buf += '			<img src="img/flowers/3.png" />';
			buf += '	</div>';
			buf += '</div>';
			break;
			
		case self.TYPE_FACEBOOK:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/facebook.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_TWITTER:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/twitter.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_SCHEDULE:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/schedule.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_LOCATION:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/map.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.locationString;
			buf += '</div>';
			break;
			
		case self.TYPE_WEATHER:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/weather.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.weatherType + ' ' + row.weatherTemperature;
			buf += '</div>';
			break;
			
		case self.TYPE_CONTACTS:
		
			buf += '<div class="log_auto" id="' + id + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/contact.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		}
		
		return buf;
	};
	
	self.dateToTimestamp = function (date) {
		var ret = "";
		ret += date.getFullYear();
		ret += "-" + (date.getMonth() + 1);
		ret += "-" + date.getDate();
		ret += " " + date.getHours();
		ret += ":" + date.getMinutes();
		return ret;
	};
	
	self.generateSample = function() {
		var sampleData = [];
		var maxSamples = 30;
		var sampleBaseDate = new Date(2013, 10, 20, 15, 10).getTime();
		
		for (i = 0 ; i < maxSamples ; i++)
		{
			var row = {};
			row.timestamp = self.dateToTimestamp(new Date(sampleBaseDate + (i * 60 * 30 * 1000)));
			row.type = parseInt((Math.random() * 10)) % 7;
			row.locationString = "Fake Location #" + i;
			row.longitude = null;
			row.latitude = null;
			row.weatherType = parseInt((Math.random() * 100)) % 48;
			row.weatherTemperature = parseInt((Math.random() * 100)) % 30;
			row.note = "1. The quick brown fox jumps over the lazy dog.\n";
			row.note += "2. The quick brown fox jumps over the lazy dog.\n";
			row.note += "3. The quick brown fox jumps over the lazy dog.\n";
			row.note += "4. The quick brown fox jumps over the lazy dog.\n";
			row.note += "5. The quick brown fox jumps over the lazy dog.\n";
			
			sampleData.push(row);
		}
		self.post(sampleData);
	};
	
	self.rowForInsert = function(obj) {
		return [
			obj.timestamp,
			obj.type,
			obj.locationString,
			obj.latitude,
			obj.longitude,
			obj.weatherType,
			obj.weatherTemperature,
			obj.note
		];
	};
	
	self.post = function (data) {
		if (!self.db) return;
		
	    self.db.transaction(function(tx) {
	        for(var i in data) {
		        tx.executeSql("INSERT INTO moment" + 
					"(timestamp, type, locationString, latitude, longitude, " + 
					" weatherType, weatherTemperature, note) values " + 
					"(?, ?, ?, ?, ?, ?, ?, ?)", 
					self.rowForInsert(data[i]), 
					self.callbackSuccess, self.callbackError);
	        }
	    });
	}

	self.update = function (id, data) {
		if (!self.db) return;
		
	    self.db.transaction(function(tx) {
	        tx.executeSql("UPDATE moment " +
	            "SET locationString=?, latitude=?, longitude=?, note=? WHERE id=?", 
	            [data.locationString, data.latitude, data.longitude, data.note, id],
				self.callbackSuccess, self.callbackError);
	    });
	};

	self.remove = function (id) {
		if (!self.db) return;
		
	    self.db.transaction(function(tx) {
	        tx.executeSql("DELETE FROM moment WHERE id=?", [id], 
				self.callbackSuccess, self.callbackError);
	    });
	};

	self.list = function(_options) {
		if (!self.db) return;
		
        var options = $.extend({
            'lastTimestamp': null, 
            'limit': 10, 
            'callback': function() {}, 
        }, _options);
    
        self.db.transaction(function(tx) {
			var query = "";
			var params = [];
			
            if(options.lastTimestamp !== null) {
                query = "SELECT * FROM moment WHERE timestamp < ? ORDER BY timestamp DESC LIMIT ?";
				params = [options.lastTimestamp, options.limit];
            } else {
                query = "SELECT * FROM moment WHERE 1 ORDER BY id LIMIT ?";
				params = [options.limit];
            }
			
			console.log(query);
            tx.executeSql(query, params, function(tx, results) {
						console.log(tx);
						console.log(results);
			            options.callback(tx, results);
			}, self.callbackError);
        });
	};
}

var PostHelper = new PostHelperPrototype();
