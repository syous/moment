package org.sourcebox.moments;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MyLocation
{
	public static MyLocation instance;

	private LocationManager locationManager;
	private LocationListener locationListener;
	private Location location;

	public Double latitude;
	public Double longitude;

	public static MyLocation getInstance()
	{
		if (instance == null)
		{
			instance = new MyLocation();
		}
		return instance;
	}

	public MyLocation()
	{}

	public class MyLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location)
		{
			if (location != null)
			{
				latitude = location.getLatitude();
				longitude = location.getLongitude();
			}
		}

		@Override
		public void onProviderDisabled(String provider)
		{}

		@Override
		public void onProviderEnabled(String provider)
		{}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)
		{}

	};
	
	public void prepare(Context context)
	{
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		if (location != null)
		{
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
	}

	public void ready()
	{
		if (locationManager != null)
		{
			locationManager.removeUpdates(locationListener);
		}
	}

	public void start(Context context)
	{
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		locationListener = new MyLocationListener();
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
		location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		if (location != null)
		{
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
	}

}
