package org.sourcebox.moments;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import com.red_folder.phonegap.plugin.backgroundservice.BackgroundService;

public class MyService extends BackgroundService
{

	private final static String TAG = MyService.class.getSimpleName();

	double latitude; // latitude
	double longitude; // longitude

	private boolean mExternalStorageAvailable = false;
	private boolean mExternalStorageWriteable = false;
	private String mExternalPath = "/moments";
	private String mLocationHistoryFilename = "locationHistory.json";

	protected void checkExternalStorage()
	{
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state))
		{
			// Can read and write the media
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		}
		else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
		{
			// Can only read the media
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		}
		else
		{
			// Can't read or write
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
	}

	private void write(String textToSave)
	{
		File root = android.os.Environment.getExternalStorageDirectory();
		File dir = new File(root.getAbsolutePath() + mExternalPath);
		if (!dir.exists()) dir.mkdirs();

		File file = new File(dir, mLocationHistoryFilename);

		try
		{
			FileOutputStream f = new FileOutputStream(file);
			PrintWriter pw = new PrintWriter(f);
			pw.println(textToSave);
			pw.flush();
			pw.close();
			f.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
			Log.i(TAG, "******* File not found. Did you"
					+ " add a WRITE_EXTERNAL_STORAGE permission to the   manifest?");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String read()
	{
		File root = android.os.Environment.getExternalStorageDirectory();
		File dir = new File(root.getAbsolutePath() + mExternalPath);
		File file = new File(dir, mLocationHistoryFilename);
		String retString = "";

		try
		{
			InputStream is = new FileInputStream(file);
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr, 8192);

			String buf;
			while (true)
			{
				buf = br.readLine();
				// readLine() returns null if no more lines in the file
				if (buf == null) break;
				retString += buf;
			}
			isr.close();
			is.close();
			br.close();

			return retString;
		}
		catch (FileNotFoundException e1)
		{
			Log.d(TAG, "file not found");
			return null;
		}
		catch (IOException e)
		{
			Log.e(TAG, "io exception");
			return null;
		}
	}

	@Override
	protected JSONObject doWork()
	{
		Log.d(TAG, "Moments - background service start");

		JSONObject result = new JSONObject();
		JSONObject jsonLocationHistory = new JSONObject();

		try
		{
			checkExternalStorage();
			if (!mExternalStorageAvailable || !mExternalStorageWriteable)
			{
				Log.e(TAG, "Storage access denied");

				result.put("Message", "Storage access denied");
				return result;
			}

			String strLocationHistory = read();
			if (!TextUtils.isEmpty(strLocationHistory))
			{
				jsonLocationHistory = new JSONObject(strLocationHistory);
			}

			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date(System.currentTimeMillis());
			String now = df.format(date);

			JSONObject jsonLocation = new JSONObject();
			jsonLocation.put("latitude", getLatitude());
			jsonLocation.put("longitude", getLongitude());
			jsonLocation.put("timestamp", now);

			jsonLocationHistory.put(now, jsonLocation);

			strLocationHistory = jsonLocationHistory.toString();
			write(strLocationHistory);

			Log.d(TAG, "Timestamp: " + now);
			Log.d(TAG, "Latitude: " + getLatitude());
			Log.d(TAG, "Longitude: " + getLongitude());

			return jsonLocation;
		}
		catch (JSONException e)
		{}

		Log.d(TAG, "Moments - background service end");

		return result;
	}
	
	String mHelloTo = "";

	@Override
	protected JSONObject getConfig()
	{
		JSONObject result = new JSONObject();

		try
		{
			result.put("HelloTo", this.mHelloTo);
		}
		catch (JSONException e)
		{}

		return result;
	}

	@Override
	protected void setConfig(JSONObject config)
	{
		try
		{
			if (config.has("HelloTo")) this.mHelloTo = config.getString("HelloTo");
		}
		catch (JSONException e)
		{}

	}

	@Override
	protected JSONObject initialiseLatestResult()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void onTimerEnabled()
	{
		// TODO Auto-generated method stub

	}

	@Override
	protected void onTimerDisabled()
	{
		// TODO Auto-generated method stub

	}
	
	private double latitudeFrom = 37.50155517264162;
	private double latitudeTo = 37.65175228874552;
	private double longitudeFrom = 126.771240234375;
	private double longitudeTo = 127.122802734375; 
	
	public double getLatitude()
	{
		return latitudeFrom + (latitudeTo - latitudeFrom) * Math.random();
	}
	
	public double getLongitude()
	{
		return longitudeFrom + (longitudeTo - longitudeFrom) * Math.random();
	}
}
