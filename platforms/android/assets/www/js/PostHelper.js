function PostHelperPrototype() {
	var self = this;
	
	self.LOCATION_CHANGE = [];
	self.WATCH_ID;

	self.POSITION = {};
	
	self.TIMELINE_ID = [];
	
	/*
	self.watchLocation = function() {
		var options = { timeout: 60 * 60 * 1000 };
		self.WATCH_ID = navigator.geolocation.watchPosition(function(position) {
			alert('Latitude: ' + position.coords.latitude + '\n' + 'Longitude : ' + position.coords.longitude + '\n');
			self.CURRENT_POSITION = position;
			self.LOCATION_CHANGE.push(position);
		}, function(error) {
			alert('code : ' + error.code + '\n' + 'message : ' + error.message + '\n');
			
			self.LOCATION_CHANGE.slice(self.LOCATION_CHANGE.length);
		}, options);
	};
    */
	
	self.setPostDatePicker = function() {
		var date = self.dateToTimestamp(new Date());
		date = date.split(' ').join('T');
		
		$("#post_datetime").val(date);
	};

	self.loadGeolocation = function() {
		navigator.geolocation.getCurrentPosition(function(position) {
			self.setLocationRelatedInformations(position);
			
		}, function(error) {
			alert('code : ' + error.code + '\n' + 'message : ' + error.message + '\n');
			
			self.setLocationRelatedInformations(self.CURRENT_POSITION);	
		});
	};
	
	self.setLocationRelatedInformations = function(position) {
		if(!position) return;
		
		$.ajax({
			type: 'get', 
			dataType: 'json',
			url: 'http://maps.googleapis.com/maps/api/geocode/json', 
			data: {
				'latlng': position.coords.latitude + ',' + position.coords.longitude, 
				'sensor': false, 
				'language': 'en', 
			}, 
		}).done(function(result) {
			if(result.results.length > 0) {
				alert(result.results[0]["formatted_address"]);
				self.POSITION.lat = result.results[0].geometry.location.lat;
				self.POSITION.lng = result.results[0].geometry.location.lng;
				$("#id_post_section_location > div > input").val(result.results[0]["formatted_address"]);
			}
		}).fail(function(err) {
			alert(err);
		});
		
		$.simpleWeather({
			lat: position.coords.latitude,
			lng: position.coords.longitude,
			unit: 'c',
			success: function (weather) {
				$("#id_post_section_weather > img").attr('src', 'img/icon/weather/' + weather.code + '.png');
				$("#id_post_section_weather > img").attr('name', weather.code);
				$("#id_post_section_weather").append('<span>  ' + weather.temp + '&deg</span>');
			},
			error: function (weather) {
				console.log(weather);
			}
		});
	};
	
	self.uploadPhoto = function() 
	{
		// $.mobile.changePage("#popup_upload_photo", { role: "popup" });
		$("#popup_upload_photo").popup("open");
	};
	
	self.capturePhoto = function() {
        navigator.camera.getPicture(self.photoSuccess, self.photoFail, {
            quilty: 50, 
            destinationType: Camera.DestinationType.FILE_URI, 
        });
	};
	
	self.getPhoto = function() {
        navigator.camera.getPicture(self.photoSuccess, self.photoFail, {
            quilty: 50, 
            destinationType: Camera.DestinationType.FILE_URI, 
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY, 
        });
	};
		
	self.photoSuccess = function(imageURI) {
        $("#popup_upload_photo").popup("close");
		
		console.log("PhotoURL : " + imageURI);
        
        $("#page_post_photo_container > .item_photo > .add_photo").parent().before(
			'<div class=item_photo>' +
			'	<img src="' + imageURI + '">' + 
			'</div>'
		);
	};
	
	self.photoFail = function(msg) {
        $("#popup_upload_photo").popup("close");
        alert('Photo Failed because: ' + msg);
	};
	
	self.postNote = function() {
		if(!$("#page_post_note").val() || $("#page_post_note").val() == "") {
			alert("Please note something");
			$("#page_post_note").focus();
			return;
		}
		
		var data = {};
		
		data.timestamp = self.dateToTimestamp(new Date($("#post_datetime").val().split('T').join(' ')));
		data.type = self.TYPE_USERPOST;
		data.locationString = $("#id_post_section_location > div > input").val() == "" ? null : $("#id_post_section_location > div > input").val();
		data.longitude = typeof self.POSITION.lng != 'undefined' ? self.POSITION.lng : null;
		data.latitude = typeof self.POSITION.lat != 'undefined' ? self.POSITION.lat : null;
		data.weatherType = $("#id_post_section_weather > img").attr('name');
		data.weatherTemperature = parseInt($("#id_post_section_weather > span").html());
		data.note = $("#page_post_note").val();
		
		if(self.db) {
			self.db.transaction(function(tx) {
				tx.executeSql("INSERT INTO moment" + 
					"(timestamp, type, locationString, latitude, longitude, " + 
					" weatherType, weatherTemperature, note) values " + 
					"(?, ?, ?, ?, ?, ?, ?, ?)", 
					self.rowForInsert(data), 
					function(tx, results) {
						console.log("Save Complete");
						
						self.savePictures(results.insertId);
						
						$.mobile.changePage('#page_main', {
							transition: 'slide', 
						});
					}, self.callbackError);
			});
		}
	};
	
	self.savePictures = function(id) {
		$("#page_post_photo_container > .item_photo > img").each(function() {
			var imageURI = $(this).attr('src');
			var imgFileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
			var directoryName = "/moment/photo/" + id;
			
			window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
	            console.log("got image file entry: " + fileEntry.fullPath); 
				
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
					fileSystem.root.getDirectory("moments", {create: true}, function(moments) {
						moments.getDirectory("photo", {create: true}, function(photo) {
							photo.getDirectory("" + id, {create: true}, function(parent) {
			                    fileEntry.copyTo(parent, null, function(fileEntry) {
										/*
										console("copied file: " + fileEntry.fullPath); 
										// !!! assumes you have an img element on page with id=largeImage 
										var largeImage = document.getElementById('largeImage'); 

										largeImage.style.display = 'block'; 
										largeImage.src = fileEntry.toURI() + "?" + (new Date()).getTime();*/
								}, function(error) {
									alert("FS failed with error code: " + error.code); 					
								}); 
							});
						});
					});	
                }, function(error) {
					alert("FS failed with error code: " + error.code); 					
				}); 
            }, function(error) {
				alert("FS failed with error code: " + error.code); 
			});
		});
	};
	
	self.initPostingPage = function() {
		$("#id_post_section_location > div > input").val("");
		$("#id_post_section_weather > img").attr('src', 'jquerymobile/sand/images/ajax-loader.gif');
		$("#id_post_section_weather > img").attr('name', '3200');
		$("#id_post_section_weather > span").remove();
		$("#page_post_note").val("");
		$("#page_post_photo_container > .item_photo > img").parent().remove();
	};
	
	self.TYPE_USERPOST = 0, 
	self.TYPE_FACEBOOK = 1, 
	self.TYPE_TWITTER = 2, 
	self.TYPE_SCHEDULE = 3, 
	self.TYPE_LOCATION = 4, 
	self.TYPE_WEATHER = 5,
	self.TYPE_CONTACTS = 6;
    
	self.MONTH = ["January", "February", "March", "April", "May", "June", "July",
				"August", "September", "October", "November", "December"];
	self.WEATHER = ["Tornado", "Tropical Storm", "Hurricane", "Severe Thunderstorms", "Thunderstorms", "Mixed rain and snow", "Mixed rain and sleet",
                    "Mixed snow and sleet", "Freezing drizzle", "Drizzle", "Freezing rain", "Showers", "Showers", "Snow flurries", "Light snow showers",
					"Blowing snow", "Snow", "Hail", "Sleet", "Dust", "Foggy", "Haze", "Smoky", "Blustery", "Windy", "Cold", "Cloudy", "Mostly cloudy (night)", 
					"Mostly cloudy (day)", "Partly cloudy (night)", "Partly cloudy (day)", "Clear (night)", "Sunny", "Fair (night)", "Fair (day)", "Mixed rain and hail", 
					"Hot", "Isolated thunderstorms", "Scattered thunderstorms", "Scattered thunderstorms", "Scattered showers", "Heavy snow", "Scattered snow showers", 
					"Heavy snow", "Partly cloudy", "Thundershowers", "Snow showers", "Isolated thundershowers"];
	self.WEATHER[3200] = "Not available";
	
	self.db = null;

	// Transaction error callback
	self.callbackError = function(tx, results) {
		console.log(results);
	};

	// Transaction success callback
	self.callbackSuccess = function (tx, results) {
		console.log(results);
	};
	
	self.init = function() {
        self.db = window.openDatabase("MOMENT_DB", "1.0", "MOMENT_DB", 200000);
        self.db.transaction(function (tx) {
			//tx.executeSql("DROP TABLE moment");
           tx.executeSql("CREATE TABLE IF NOT EXISTS moment (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "timestamp TIMESTAMP NOT NULL, " +
                "type INTEGER(3) NOT NULL, " +
                "locationString VARCHAR(255), " +
                "longitude FLOAT, " +
                "latitude FLOAT, " +
                "weatherType INTEGER(6), " +
                "weatherTemperature FLOAT, " +
                "note VARCHAR(255))"
				, [], 
				function (tx, results) {
					self.list({
						callback: function (tx, results) {
							if (is_test && results.rows && results.rows.length == 0)
							{
								self.generateSample();
							}
	
							self.callbackList(tx, results);
						},
					});
                }, self.callbackError);
		});
	};
	
	self.callbackList = function(tx, results) {
		if (results.rows && results.rows.length > 0)
		{
			var html = '';
			var newDates = [];
			for (var i = 0 ; i < results.rows.length ; i++)
			{
				var item = results.rows.item(i);
				
				if ($.inArray(item.create_date, newDates) < 0 && 
					$("#id_main_list > div[date='" + item.create_date + "']").length < 1)
				{
					var date = new Date(item.timestamp);
					var timestamp = date.getFullYear() + "-" + ((date.getMonth()+1) < 10 ? "0" : "") + (date.getMonth()+1) + "-" + 
									(date.getDate() < 10 ? "0" : "") + date.getDate() + " 23:59:59";

					html += '<div  class="divider_date" date="' + item.create_date + '" timestamp="' + timestamp + '">';
					html += '	<div class="divider_content">' + self.MONTH[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear() + '</div>';
					html += '	<div class="divider_line"></div>';
					html += '</div>';

					newDates.push(item.create_date);
				}
				
				if($("#id_main_list > div[id='id_" + item.timestamp + "_" + item.id + "']").length < 1)
					html += self.rowToHtml(item);
			}
			
			$("#id_main_list").prepend(html);
			
			self.addDeleteEvent();
			
			$("#id_main_list > div").tsort({
				attr: 'timestamp',
				order: 'desc', 
			});
		}
	};
	
	self.rowToHtml = function(row) {
		var buf = "";
		
		var id = "id_" + row.timestamp + "_" + row.id;
		
		switch (row.type)
		{
		case self.TYPE_USERPOST:
			buf += '<div class="log ui-shadow" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_date">' + row.timestamp + '</div>';
			buf += '	<div class="log_weather">' + self.WEATHER[row.weatherType] + ', ' + row.weatherTemperature + '&degC</div>';
			buf += '		<div class="log_content">';
			buf += row.note;
			buf += '		</div>';
			buf += '		<div class="log_picture">';
			buf += '			<img src="img/flowers/1.png" />';
			buf += '			<img src="img/flowers/2.png" />';
			buf += '			<img src="img/flowers/3.png" />';
			buf += '	</div>';
			buf += '</div>';
			break;
			
		case self.TYPE_FACEBOOK:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/facebook.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_TWITTER:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/twitter.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_SCHEDULE:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/schedule.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		case self.TYPE_LOCATION:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/map.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.locationString;
			buf += '</div>';
			break;
			
		case self.TYPE_WEATHER:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/weather/' + row.weatherType + '.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += self.WEATHER[row.weatherType] + ' ' + row.weatherTemperature + '&degC';
			buf += '</div>';
			break;
			
		case self.TYPE_CONTACTS:
		
			buf += '<div class="log_auto" id="' + id + '" timestamp="' + row.timestamp + '">';
			buf += '	<div class="log_delete">X</div>';
			buf += '	<img src="img/icon/contact.png" width="20" align="absmiddle"/>';
			buf += '	<div class="log_auto_timestamp">' + row.timestamp + '</div>';
			buf += row.note;
			buf += '</div>';
			break;
			
		}
		
		return buf;
	};
	
	self.dateToTimestamp = function (date) {
		var ret = "";
		ret += date.getFullYear();
		ret += "-" + (((date.getMonth() + 1) < 10) ? "0" : "") + (date.getMonth() + 1);
		ret += "-" + ((date.getDate() < 10) ? "0" : "") + date.getDate();
		ret += " " + ((date.getHours() < 10) ? "0" : "") + date.getHours();
		ret += ":" + ((date.getMinutes() < 10) ? "0" : "") + date.getMinutes();
		return ret;
	};
	
	self.addDeleteEvent = function() {
		$("#id_main_list > div > div.log_delete").click(function() {
			var id = $(this).parent().attr('id');
			console.log(id);
			self.remove(id.split('_')[2]);
			$('#' + id).remove();
		});
	}
	
	self.generateSample = function() {
		var sampleData = [];
		var maxSamples = 30;
		var sampleBaseDate = new Date(2013, 10, 20, 15, 10).getTime();
		
		for (var i = 0 ; i < maxSamples ; i++)
		{
			var row = {};
			row.timestamp = self.dateToTimestamp(new Date(sampleBaseDate + (i * 60 * 60 * 12 * 1000)));
			row.type = parseInt((Math.random() * 10), 10) % 7;
			row.locationString = "Fake Location #" + i;
			row.longitude = null;
			row.latitude = null;
			row.weatherType = parseInt((Math.random() * 100), 10) % 48;
			row.weatherTemperature = parseInt((Math.random() * 100), 10) % 30;
			row.note = "1. The quick brown fox jumps over the lazy dog.\n";
			row.note += "2. The quick brown fox jumps over the lazy dog.\n";
			row.note += "3. The quick brown fox jumps over the lazy dog.\n";
			row.note += "4. The quick brown fox jumps over the lazy dog.\n";
			row.note += "5. The quick brown fox jumps over the lazy dog.\n";
			
			sampleData.push(row);
		}
		self.post(sampleData);
	};
	
	self.rowForInsert = function(obj) {
		return [
			obj.timestamp,
			obj.type,
			obj.locationString,
			obj.latitude,
			obj.longitude,
			obj.weatherType,
			obj.weatherTemperature,
			obj.note
		];
	};
	
	self.post = function (data) {
		if (!self.db) return;
		
        self.db.transaction(function(tx) {
            for(var i in data) {
                tx.executeSql("INSERT INTO moment" + 
					"(timestamp, type, locationString, latitude, longitude, " + 
					" weatherType, weatherTemperature, note) values " + 
					"(?, ?, ?, ?, ?, ?, ?, ?)", 
					self.rowForInsert(data[i]), 
					self.callbackSuccess, self.callbackError);
            }
        });
	};

	self.update = function (id, data) {
		if (!self.db) return;
		
        self.db.transaction(function(tx) {
            tx.executeSql("UPDATE moment " +
                "SET locationString=?, latitude=?, longitude=?, note=? WHERE id=?", 
                [data.locationString, data.latitude, data.longitude, data.note, id],
                self.callbackSuccess, self.callbackError);
        });
	};

	self.remove = function (id) {
		if (!self.db) return;
		
        self.db.transaction(function(tx) {
            tx.executeSql("DELETE FROM moment WHERE id=?", [id], 
				self.callbackSuccess, self.callbackError);
        });
	};

	self.list = function(_options) {
		if (!self.db) return;
		
        var options = $.extend({
            'lastTimestamp': null, 
            'limit': 10, 
            'callback': function() {}, 
        }, _options);
    
        self.db.transaction(function(tx) {
			var query = "";
			var params = [];
			
            if(options.lastTimestamp !== null) {
                query = "SELECT m.* ";
				query += ", DATE(m.timestamp) AS create_date, TIME(m.timestamp) AS create_time ";
				query += ", strftime('%s', m.timestamp) AS unix_timestamp ";
				query += "FROM moment AS m WHERE timestamp < ? ORDER BY timestamp DESC LIMIT ?";
				params = [options.lastTimestamp, options.limit];
            } else {
                query = "SELECT m.*";
				query += ", DATE(m.timestamp) AS create_date, TIME(m.timestamp) AS create_time ";
				query += ", strftime('%s', m.timestamp) AS unix_timestamp ";
				query += "FROM moment AS m WHERE 1 ORDER BY id LIMIT ?";
				params = [options.limit];
            }
			
			console.log(query);
            tx.executeSql(query, params, function(tx, results) {
						console.log(tx);
						console.log(results);
                        options.callback(tx, results);
			}, self.callbackError);
        });
	};
}

var PostHelper = new PostHelperPrototype();
