
$(document).on('mobileinit', function() {
	$.support.cors = true;
	$.mobile.allowCrossDomainPages = true;
});

var is_desktop = true;
var is_test = true;
var initEvent = is_desktop ? 'ready' : 'deviceready';

$(document).on(initEvent, function() {
	ServiceHelper.init();
	PostHelper.init();
//	PostHelper.watchLocation();

	$("#page_imagepopup").on('click', function() 
	{
		history.back();
		return false;
	});
	
	$("#id_log_weather").on('click', function() 
	{
		window.alert('updating weather');
		updateWeather();
	});
	
	$(".log_picture > img").on('click', function() 
	{
		$.mobile.changePage('#page_imagepopup', {
			transition: 'pop',
		});
		
		var image = new Image();
		image.src = $(this).attr("src");
		$("#page_imagepopup").html('').append(image);
	});
	
	$("#page_btn_post").on('click', function() {
		PostHelper.postNote();
	});
	
});

$(document).bind("pagechange", function(event, data) {
	if (typeof data.absUrl != 'undefined' && data.absUrl.match(/\#page_post$/)) {
		PostHelper.loadGeolocation();
		PostHelper.setPostDatePicker();
	} else if (typeof data.absUrl != 'undefined' && data.absUrl.match(/\#page_main$/)) {
		PostHelper.initPostingPage();
	}
});



function updateWeather() {
	
	PostHelper.init();
	
	$.simpleWeather({
		lat: 37.56,
		lng: 126.93,
		unit: 'c',
		success: function (weather) {
			
			var htmlImg = "<img src='"  + weather.thumbnail + "' />" + weather.thumbnail;
			$("#id_weather").html(htmlImg);
			console.log(weather);			
		},
		error: function (weather) {
			$("#id_weather").html("error");
		}
	});
}
